import sys

class DirectGraph:

    def __init__(self, n):

        self._n = n
        self._out = {}
        self._in = {}
        self._cost = {}
        self._list = []
        self._vertices = 0
        for i in range(0, n):
            self._in[i] = []
            self._out[i] = []
        self._graph = {}
        self._matrice = [[0 for column in range(n)] \
                      for row in range(n)]

    '''
    def validateOut(self, x):
        if x in range(len(self._out)):
            if -1 == self._out[x][0]:
                return False
        return True
    def validateIn(self, x):

        if x in range(len(self._in)):
            if -1 == self._in[x][0]:
                return False
        return True
    '''
    def addLaGraf(self, a, b, c):
        Lista = {b:c}
        if a not in self._graph.keys():
            self._graph[a] = Lista
        else:
            self._graph[a][b] = c

    def addLaMatrice(self, a, b):
        self._matrice[a][b] = 1
        self._matrice[b][a] = 1

    def numberOfVertices(self):

        nr = 0
        for i in range(0, len(self._out)):
            print(len(self._out))
            if -1 != self._out[i][0]:
                nr += 1
        return nr
    '''
    def addVertexs(self, x):

        if x > 0:
            for j in range(len(self._out) + 1, x):
                self._out.update({j: [-2]})
                self._in.update({j: [-2]})
            self._out.upd ate({x: [-2]})
            self._in.update({x: [-2]})
            return 1
        else:
            return -1
    '''

    def removeVertex(self, x):

        if x in self._out:
            self._out[x] = -1


    def isEdge(self, x, y):

        return y in self._out[x]

    def addEdges(self, x, y, cost):

        if x in self._out.keys() and y in self._out[x] or y in self._in.keys() and y in self._in[x]:
            return -1

        if x not in self._out.keys():
            self._out[x] = []

        if y not in self._in.keys():
            self._in[y] = []

        self._out[x].append(y)
        self._in[y].append(x)
        self._cost[x,y] = cost

        return 1
    def vertOutDegree(self, x):

        if self._out[x][0] == 0:
            return 0

        return len(self._out[x]) - 1

    def vertInDegree(self, x):


        if self._out[x][0] == 0:
            return 0

        return len(self._in[x]) - 1

    def iterateOutDegree(self, x):


        if len(self._in[x]) == 1:
            return 0

        l = []

        for i in range(1, len(self._out[x])):
            z = (x, self._out[x][i])
            l.append([self._out[x][i], self._cost[z]])

        return l

    def iterateInDegree(self, x):


        if len(self._in[x]) == 1:
            return 0

        l = []

        for i in range(1, len(self._in[x])):
            z = (self._in[x][i], x)
            l.append([self._in[x][i], self._cost[z]])

        return l

    def bfs_paths(self, start, goal):

        queue = [(start, [start])]
        while queue:
            (vertex, path) = queue.pop(0)
            for next in set(self._out[vertex]) - set(path):
                if next == goal:
                    yield path + [next]
                else:
                    queue.append((next, path + [next]))

    def sortest_path(self, start, goal):
        try:
            return next(self.bfs_paths(start, goal))
        except StopIteration:
            return None

    '''
    Dijkstra algorithm for shortest path
    '''
    def _createMatrix(self,vertices):
        new_list=[]
        for j in range(0, int(vertices)):
            for i in range(0,int(vertices)):
                new_list.append(0)
            self._list.append(new_list)
            new_list=[]

    def _updateMatrix(self,vertex1,vertex2,cost):
        self._list[vertex1][vertex2]=cost

    def _updateNrVertices(self,vertices):
        self._vertices=vertices

    def printPath(self, parent, j,solution):

        self.printPath(parent, parent[j],solution)
        solution.append(j)

    # A utility function to print the constructed distance
    # array

    def printSolution(self, dist, parent):
        return dist,parent

    def minDistance(self, dist, sptSet):
        # Initialize min value and min_index as -1
        min = float("Inf")
        min_index = -1
        # from the dist array,pick one which has min value and is till in queue

        for i in range(self._vertices):
            if dist[i] < min and sptSet[i]==False:
                min = dist[i]
                min_index = i
        return min_index

    # Funtion that implements Dijkstra's single source
    # shortest path algorithm for a graph represented
    # using adjacency matrix representation
    def dijkstra(self,src):

        dist = [float("Inf")] * self._vertices
        dist[src] = 0
        Set = [False] * self._vertices
        parent = [0]*self._vertices
        for cout in range(self._vertices):

            # Pick the minimum distance vertex from
            # the set of vertices not yet processed.
            # u is always equal to src in first iteration
            u = self.minDistance(dist, Set)

            # Put the minimum distance vertex in the
            # shotest path tree
            Set[u] = True

            # Update dist value of the adjacent vertices
            # of the picked vertex only if the current
            # distance is greater than new distance and
            # the vertex in not in the shotest path tree
            for v in range(self._vertices):
                if self._list[v][u] > 0 and Set[v] == False and dist[v] > dist[u] + self._list[v][u]:
                    dist[v] = dist[u] + self._list[v][u]
                    parent[v] = u

        # print the constructed distance array
        return self.printSolution(dist, parent)


    #lab 4
    def popmin(self, pqueue):
        # A (ascending or min) priority queue keeps element with
        # lowest priority on top. So pop function pops out the element with
        # lowest value. It can be implemented as sorted or unsorted array
        # (dictionary in this case) or as a tree (lowest priority element is
        # root of tree)
        lowest = 10000
        keylowest = None
        for key in pqueue:
            if pqueue[key] < lowest:
                lowest = pqueue[key]
                keylowest = key
        del pqueue[keylowest]
        return keylowest

    def prim(self, root):
        pred = {}  # pair {vertex: predecesor in MST}
        key = {}  # keep track of minimum weight for each vertex
        pqueue = {}  # priority queue implemented as dictionary

        for v in self._graph:
            pred[v] = -1
            key[v] = 1000
        key[root] = 0
        for v in self._graph:
            pqueue[v] = key[v]

        while pqueue:
            u = self.popmin(pqueue)
            for v in self._graph[u]:  # all neighbors of v
                if v in pqueue and self._graph[u][v] < key[v]:
                    pred[v] = u
                    key[v] = self._graph[u][v]
                    pqueue[v] = self._graph[u][v]
        return pred

    #lab 5

    ''' Check if this vertex is an adjacent vertex 
            of the previously added vertex and is not 
            included in the path earlier '''

    def isSafe(self, v, pos, path):
        # Check if current vertex and last vertex
        # in path are adjacent
        if self._matrice[path[pos - 1]][v] == 0:
            return False

        # Check if current vertex not already in path
        for vertex in path:
            if vertex == v:
                return False

        return True

    # A recursive utility function to solve
    # hamiltonian cycle problem
    def hamCycleUtil(self, path, pos):

        # base case: if all vertices are
        # included in the path
        if pos == self._n:
            # Last vertex must be adjacent to the
            # first vertex in path t+o make a cyle
            if self._matrice[path[pos - 1]][path[0]] == 1:
                return True
            else:
                return False

        # Try different vertices as a next candidate
        # in Hamiltonian Cycle. We don't try for 0 as
        # we included 0 as starting point in in hamCycle()
        for v in range(1, self._n):

            if self.isSafe(v, pos, path) == True:

                path[pos] = v

                if self.hamCycleUtil(path, pos + 1) == True:
                    return True

                # Remove current vertex if it doesn't
                # lead to a solution
                path[pos] = -1

        return False

    def hamCycle(self):
        path = [-1] * self._n

        ''' Let us put vertex 0 as the first vertex 
            in the path. If there is a Hamiltonian Cycle, 
            then the path can be started from any point
            of the cycle as the graph is undirected '''
        path[0] = 0

        if self.hamCycleUtil(path, 1) == False:
            print("Solution does not exist\n")
            return False

        self.printCycle(path)
        return True

    def printCycle(self, path):
        print("Solution Exists: Following is one Hamiltonian Cycle")
        for vertex in path:
            print(vertex)
        print(path[0], "\n")