class UI:

    def __init__(self, ctrl):
        self._ctrl = ctrl

    def numberVertices(self):

        x = self._ctrl.numberOfVertices()
        if (x == 0):
            print("There are no vertices!")
        else:
            print("The number of vertices is ", repr(x))

    def edgeBetween(self):
        x = int(input("Enter the first vertex: "))
        y = int(input("Enter the second vertex: "))
        z = self._ctrl.isEdge(x, y)
        if z == -1:
            print("There is no edge between " + repr(x) + " and " + repr(y))
        else:
            print("There is an edge between " + repr(x) + " and " + repr(y))

    def inAndOut(self):
        x = int(input("Enter the vertex: "))
        y = self._ctrl.vertOutDegree(x)
        z = self._ctrl.vertInDegree(x)
        if y == -1:
            print("The vertex does not exist")
        elif (y == 0):
            print("No outbound edges")
        else:
            print("The outdegree is ", repr(y))
        if z == 0:
            print("No inbound edges")
        else:
            print("The indegree is ", repr(z))

    def iterateOut(self):
        x = int(input("Enter the vertex: "))
        z = self._ctrl.iterateOutDegree(x)
        if z == -1:
            print("The vertex does not exist")
        elif (z == 0):
            print("No outbound edges")
        else:
            for i in range(0, len(z)):
                print("Edge " + repr(x) + " " + repr(z[i][0]) + " with cost " + repr(z[i][1]))

    def addEdge(self):
        x = int(input("Enter the 1st vertex: "))
        y = int(input("Enter the 2nd vertex: "))
        z = int(input("Enter the cost:  "))
        self._ctrl.addEdge(x, y, z)

    def addVert(self):
        x = int(input("Enter the vertex: "))
        self._ctrl.addVertex(x)

    def iterateIn(self):
        x = int(input("Enter the vertex: "))
        z = self._ctrl.iterateInDegree(x)
        if z == -1:
            print("The vertex does not exist")
        elif (z == 0):
            print("No inbound edges")
        else:
            for i in range(0, len(z)):
                print("Edge " + repr(z[i][0]) + " " + repr(x) + " with cost " + repr(z[i][1]))

    def sortest_path(self):
        start = int(input("Enter the first vertex: "))
        goal = int(input("Enter the second vertex: "))
        print(self._ctrl.sortest_path(start, goal))

    def dijkstra(self):
        vertex1 = int(input("Give first vertex:"))
        vertex2 = int(input("Give second vertex:"))
        path = []
        save = []
        dist, path = self._ctrl.dijkstra(vertex2)
        if dist[vertex1] == "Inf":
            print("There is no path")
        else:
            print("Lowest cost is:", dist[vertex1])

        for i in range(0, len(path)):
            if path[i] == vertex1:
                index = i
        save.append(vertex1)
        save.append(path[vertex1])
        i = path[vertex1]
        while path[i] != vertex2:
            save.append(path[i])
            i = path[i]
        save.append(vertex2)

        print(save)

    def prim(self):
        pred = self._ctrl.prim(0)
        for v in pred: print("%s: %s" % (v, pred[v]))

    def ham(self):
        self._ctrl.hamCycle()

    def menu(self):
        while True:
            cmds = {"1": self.numberVertices, "2": self.edgeBetween, "3": self.inAndOut, "4": self.iterateOut,
                    "5": self.iterateIn, "6": self.addVert, "7": self.addEdge, "8": self.sortest_path, "9": self.dijkstra, "10": self.prim, "11": self.ham}
            com = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "0"]
            print("1 - Number of vertices\n", "2 - Is edge between\n", "3 - In and out degree\n", "4 - Iterate out\n", "5 - Iterate in\n", "6 - Add vertex\n", "7 - Add edge\n", "8 - Lowest length path\n", "9 - Backwards Dijkstra\n", "10 - Prim's algorithm\n", "11 - Check for Hamiltonian Cycle\n", "0 - Exit\n")
            cmd = input("Enter command: ")
            if cmd in com:
                if cmd == "0":
                    print("Exit...")
                    return
                else:
                    cmds[cmd]()
            else:
                print("Invalid command!\n")
