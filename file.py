from graf import DirectGraph


class DefineFile(DirectGraph):

    def __init__(self, fileName="testmic"):

        f = open(fileName, "r")
        line = f.readline().strip()
        attrs = line.split(" ")
        n = int(attrs[0])
        f.close()
        DirectGraph.__init__(self, n)
        self.__fName = fileName
        self.__loadFromFile()

    def __loadFromFile(self):

        f = open(self.__fName, "r")
        line = f.readline().strip()
        attrs = line.split(" ")
        n = int(attrs[0])
        m = int(attrs[1])
        DirectGraph._updateNrVertices(self, int(attrs[0]))
        DirectGraph._createMatrix(self, int(attrs[0]))
        line = f.readline().strip()
        while line != "":
            attrs = line.split(" ")
            #print(DirectGraph.addEdges(self, int(attrs[0]), int(attrs[1]), int(attrs[2])))
            DirectGraph.addEdges(self, int(attrs[0]), int(attrs[1]), int(attrs[2]))
            DirectGraph._updateMatrix(self, int(attrs[0]), int(attrs[1]), int(attrs[2]))
            DirectGraph.addLaGraf(self, int(attrs[0]), int(attrs[1]), int(attrs[2]))
            DirectGraph.addLaGraf(self, int(attrs[1]), int(attrs[0]), int(attrs[2]))
            DirectGraph.addLaMatrice(self, int(attrs[1]), int(attrs[0]))
            line = f.readline().strip()
        f.close()


    def addVertex(self, x):
        #DirectGraph.addVertexs(self, x)
        pass

    def addEdge(self, x, y, cost):
        DirectGraph.addEdges(self, x, y, cost)
